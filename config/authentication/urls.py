from django.urls import path
from django.contrib.auth import views as auth_views
from . import views

app_name = 'authentication'

urlpatterns = [
    path('', views.home, name='home'),
    path('signup', views.signup, name='signup'),
    path('signin', auth_views.LoginView.as_view(template_name='authentication/signin.html'), name='signin'),
    path('signout', auth_views.LogoutView.as_view(template_name='authentication/signout.html'), name='signout'),    
]
