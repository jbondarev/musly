from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.contrib import messages
from .forms import UserSignUpForm
# Create your views here.


def home(request):
    
    if request.user.is_authenticated:
        if request.user.is_superuser:
            return redirect('admin:index')
        return redirect('events:upcoming_events')
        
    if request.method == 'POST':
        form = UserSignUpForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            new_user = authenticate(username=form.cleaned_data['username'],
                                    password=form.cleaned_data['password1'])
            login(request, new_user)
            return redirect('authentication:home')
    else:
        form = UserSignUpForm()
        
    return render(request, 'authentication/home.html', {'form': form})


def signup(request):
    if request.method == 'POST':
        form = UserSignUpForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            new_user = authenticate(username=form.cleaned_data['username'],
                                    password=form.cleaned_data['password1'])
            login(request, new_user)
            return redirect('authentication:home')
    else:
        form = UserSignUpForm()
    return render(request, 'authentication/signup.html', {'form': form})