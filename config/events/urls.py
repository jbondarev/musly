from django.urls import path
from . import views
app_name = "events"

urlpatterns = [
    path('', views.upcoming_events, name='upcoming_events'),
    path('add-event', views.add_event, name='add_event'),
    path('upcoming-events', views.user_upcoming_events_list, name='upcoming-events'),
    path('following-list', views.user_following_list, name='following-list'),
    path('users', views.UserAPIView.as_view(), name='users'),
    path('attendances', views.AttendanceAPIView.as_view(), name='attendances'),
    path('followings', views.FollowingAPIView.as_view(), name='followings'),
    
]
