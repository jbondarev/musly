from django.conf import settings
from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Attendance, Following
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email')
        
        
class AttendanceSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Attendance
        fields = ('attendee', 'event')
        
    def create(self, data):
        attendee_id = data.get('attendee')
        event_id = data.get('event')
        return Attendance.objects.create(attendee=attendee_id, event=event_id)
    
    
class FollowingSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Following
        fields = ('follower', 'followee')
        
    def create(self, data):
        follower_id = data.get('follower')
        followee_id = data.get('followee')
        return Following.objects.create(follower=follower_id, followee=followee_id)