class EventWrapper(object):

    def __init__(self, id: str, artist: str, month: str, day: str, genre: str) -> None:
        self.id: str = id
        self.artist: str = artist
        self.month: str = month
        self.day: str = day
        self.genre: str = genre
        