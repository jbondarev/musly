from django import forms
from django.http import Http404
from .models import Event
class AddEventForm(forms.ModelForm):
    
    class Meta:
        model = Event
        fields = ['datetime', 'meet_place', 'genre']
        widgets = { 
            'genre  ': forms.Select(choices=model.objects.all().__str__(), attrs={'class': 'form-control'}),
        }
        labels = {
            'datetime': 'Date and time',
            'meet_place': 'Meet place',
            'genre': 'Genre',
        }
        