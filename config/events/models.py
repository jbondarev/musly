from django.conf import settings
from django.db import models
from django.utils import timezone
from datetime import date as date_
# Create your models here.

    
class Genre(models.Model):
    name: models.CharField = models.CharField(max_length=255) 
    
    def __str__(self) -> str:
        return self.name
    
    
class Event(models.Model):
    artist: models.ForeignKey = models.ForeignKey(settings.AUTH_USER_MODEL,
                                                  on_delete=models.CASCADE)
    datetime: models.DateTimeField = models.DateTimeField(default=timezone.now)
    meet_place: models.TextField = models.CharField(max_length=255)
    genre: models.ForeignKey = models.ForeignKey(Genre, 
                                                 on_delete=models.CASCADE)
    
    def __str__(self) -> str:
        return f"[{self.genre}] {self.artist}:{self.meet_place}, {self.datetime}"



class Attendance(models.Model):
    """
    Many-to-many realisation of model between Users and events
    """
    attendee: models.ForeignKey = models.ForeignKey(settings.AUTH_USER_MODEL,
                                                  on_delete=models.CASCADE)
    event: models.ForeignKey = models.ForeignKey(Event,
                                                  on_delete=models.CASCADE)
        
    class Meta:
        unique_together = ('attendee', 'event')
        

class Following(models.Model):
    """
    Many-to-many realisation of model between follower and followee
    """
    
    follower: models.ForeignKey = models.ForeignKey(settings.AUTH_USER_MODEL,
                                                  on_delete=models.CASCADE,
                                                  related_name='follower_id')
    followee: models.ForeignKey = models.ForeignKey(settings.AUTH_USER_MODEL,
                                                  on_delete=models.CASCADE,
                                                  related_name='followee_id')
    
    class Meta:
        unique_together = ('follower', 'followee')