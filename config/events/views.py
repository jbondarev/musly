from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator 
# python
from copy import deepcopy
# rest
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
# local
from .forms import AddEventForm
from .models import Event, Attendance, Following
from .helper import EventWrapper
from .serializers import UserSerializer, AttendanceSerializer, FollowingSerializer
# Create your views here.

@login_required()
def add_event(request):
    if request.method == 'POST':
        form = AddEventForm(request.POST)
        if form.is_valid():
            new_event = form.save(commit=False)
            new_event.artist = request.user
            new_event.save()
            return redirect('events:upcoming_events')
    else:
        form = AddEventForm()
    return render(request, 'events/add_event.html', {'form': form})


@login_required()
def upcoming_events(request):
    events = Event.objects.all()
    calendar = [EventWrapper(str(event.id),
                             str(event.artist),
                             str(event.datetime.date().strftime('%b')),
                             str(event.datetime.date().day),
                             str(event.genre)) 
                             for event in events]
    return render(request, 'events/upcoming_events.html', {'calendar': calendar})


@login_required()
def user_upcoming_events_list(request):
    attendance_objects = Attendance.objects.filter(attendee=request.user)
    events = [attendance_object.event for attendance_object in attendance_objects]
    calendar = [EventWrapper(str(event.id),
                             str(event.artist),
                             str(event.datetime.date().strftime('%b')),
                             str(event.datetime.date().day),
                             str(event.genre)) 
                             for event in events]
    return render(request, 'events/user_upcoming_events.html', {'calendar': calendar})


@login_required()
def user_following_list(request):
    following_objects = Following.objects.filter(follower=request.user)
    following = [following_object.followee for following_object in following_objects]
    return render(request, 'events/user_following_list.html', {'following': following})




class UserAPIView(APIView):
    
    @method_decorator(login_required)
    def get(self, request):
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)


class AttendanceAPIView(APIView):
    
    @method_decorator(login_required)
    def get(self, request):
        attendance = Attendance.objects.all()
        serializer = AttendanceSerializer(attendance, many=True)
        return Response(serializer.data)
    
    
    @method_decorator(login_required)
    def post(self, request):
        data = deepcopy(request.data)
        data['attendee'] = request.user.id
        serializer = AttendanceSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    
class FollowingAPIView(APIView):
     
    @method_decorator(login_required)
    def get(self, request):
        following = Following.objects.all()
        serializer = FollowingSerializer(following, many=True)
        return Response(serializer.data)
    
    
    @method_decorator(login_required)
    def post(self, request):
        data = deepcopy(request.data)
        data['follower'] = request.user.id
        print(data)
        try: 
            data['followee'] = (User.objects.get(username=data['followee'])).id
        except Exception as e:
            return Response(data={"Exception": e}, status=status.HTTP_400_BAD_REQUEST)
        print(data)
        serializer = FollowingSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)