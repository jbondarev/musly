from django.contrib import admin

from .models import Genre, Event
# Register your models here.

admin.site.register(Genre)
admin.site.register(Event)